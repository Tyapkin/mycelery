import os

CONNECTION = os.getenv('CONNECTION', '')

LOGS_DIR = os.path.abspath(os.getenv('LOGS', ''))
FILES_DIR = os.path.abspath(os.getenv('FILES', ''))
