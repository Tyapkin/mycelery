from __future__ import absolute_import

from celery import Celery

from my_app.settings.common import CONNECTION
from my_app.settings.apps import APPS_LIST

app = Celery('my_app', broker=CONNECTION)
app.autodiscover_tasks(lambda: APPS_LIST)
app.conf.update(result_expires=3600)

if __name__ == '__main__':
    app.start()
