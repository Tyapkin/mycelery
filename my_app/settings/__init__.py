import os
from dotenv import load_dotenv

load_dotenv(os.path.join(os.path.abspath('config'), '.env'))
