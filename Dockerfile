FROM python:3.8-alpine
WORKDIR /build
RUN apk --no-cache -U add alpine-sdk python3-dev bash && rm -rf /var/cache/apk/*
RUN mkdir config
RUN mkdir logs
RUN mkdir files
COPY config/app_env /build/config/.env
COPY requirements.txt /build/
COPY my_app /build/my_app
RUN pip install -r requirements.txt