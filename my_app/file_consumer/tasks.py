from __future__ import absolute_import
import os
import logging

from my_app.celery import app
from my_app.settings.common import FILES_DIR, LOGS_DIR

logging.basicConfig(
    filename=os.path.join(LOGS_DIR, 'file_consumer.log'),
    format='%(asctime)s %(message)s',
    level=logging.DEBUG
)


@app.task
def file_remover(filename: str) -> None:
    try:
        filepath = os.path.join(FILES_DIR, filename)
        os.remove(filepath)
    except Exception as exc:
        logging.error(exc)
        pass
